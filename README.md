# Overhang-B-Gone Nim

Rotate STL meshes to minimize overhangs for easier 3D printing. See [documentation](https://pawelstrzebonski.gitlab.io/overhang-b-gone-nim).

Note: This project is in early stages and likely has bugs or missing features.

## Features

* Rotates meshes
* Supports binary STL files

## Usage/Installation

This repository contains the Nimble project at top level with Nim source code in the `src/` directory. It can be built using `nimble build`.

If you have [Nix](https://nixos.org/) then you can use the included `shell.nix` to start a shell with installed dependencies for development, or else you can use the `default.nix` script to build/install a specific version of this application. Note that `default.nix` specifies the commit to be built, so it may need to be updated to build the latest version.
