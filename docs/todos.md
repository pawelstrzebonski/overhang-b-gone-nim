# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* Application improvements:
	* expose more application settings via CLI
	* support more file formats (namely ASCII STL files)
* Algorithm improvements:
	* probabilistically sampled triangles for scaling to huge STLs
	* account for on-build-plate faces during optimization
	* other/better heuristics
* General code/project improvements:
	* tests
	* CI
	* build documentation from source
