# Package

version       = "0.1.0"
author        = "Pawel Strzebonski"
description   = "Rotate STL meshes to minimize overhang for easier 3D printing."
license       = "AGPL-3.0-or-later"
srcDir        = "src"
bin           = @["overhangbgonenim"]


# Dependencies

requires "nim >= 1.6.8"
requires "arraymancer >=0.7.19"
