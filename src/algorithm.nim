import arraymancer
import std/math

proc naiveRotationOptimize*(rotations, normals, surfaceareas, centers: Tensor[
        float32], thresholddegrees: float32): Tensor[float32] =
    ## Naive/brute-force rotation optimization
    # Sanity check matrix shapes/sizes
    assert rotations.shape[1] == 3 and normals.shape[1] == 3 and centers.shape[
            1] == 3
    assert normals.shape[0] == surfaceareas.shape[0] and normals.shape[0] ==
            centers.shape[0]

    ## Calculate rotation scores based on total overhang
    # Calculate dot product between each pair of rotations/normals
    let rotnorm = rotations*normals.transpose()
    # Calculate threshold angle as cosine (equivalent to dot product of vectors)
    let thresholdcosoverhang = cos(degToRad(thresholddegrees))
    # Binary array indicating failing pairs
    let isaboveoverhangthreshold = rotnorm >. thresholdcosoverhang
    # Create equivalent numeric matrix
    var scoremask = zeros_like[float32](rotnorm)
    scoremask.masked_fill(isaboveoverhangthreshold, 1.0f)
    # Multiply to have total surface area of failing triangles
    let scores = scoremask*surfaceareas

    ## Calculate adjustment for faces close to print surface
    # Calculate projections of triangle centers onto rotations
    let rotcent = rotations*centers.transpose()
    # https://en.wikipedia.org/wiki/Steradian
    # Unit sphere has 4pi steradians, let's split accross N points on sphere surface
    # so each point has 4pi/N steradians. To get half-full-angle of cone around this point
    # we estimate 1-2/N=cos(theta), so theta=arccos(1-2/N). This should give an estimate
    # for the average angle between adjacent rotation vectors.
    let thresholdcosparallelish = cos(arccos(1-2/rotations.shape[0])*2)
    echo thresholdcosparallelish
    # Identify normals close to parallel to rotations
    let isaboveparallelishthreshold = rotnorm >. thresholdcosparallelish
    # Get indices of triangles with centers closest to print surface
    let lowestcenters = max(rotcent, axis = 1)
    let highestcenters = min(rotcent, axis = 1)
    # Determine if lowest triangle is parallel to print surface
    assert lowestcenters.shape[0] == rotations.shape[0]
    # Is each triangle among the lowest in any given rotation
    #TODO: how to select threshold?
    let islowest = abs(rotcent -. lowestcenters) <. abs(
            highestcenters-lowestcenters) /. 1000
    # Calculate surface area of all triangles parallel to print surface that are lowest in object
    let surfaceadjustment = (isaboveparallelishthreshold and islowest).astype(
            float32) * surfaceareas
    assert surfaceadjustment.shape == scores.shape
    #TODO: verify that this section works

    assert scores.shape[0] == rotations.shape[0]
    # Find index of lowest area rotation
    # Subtract out surfaceadjustment to not count faces that are likely
    # OK to print on print surface without supports
    let bestidx = argmin(scores -. surfaceadjustment, axis = 0)
    # Select lowest rotation vector
    let bestrotation = rotations[bestidx[0], _].squeeze(0)
    assert bestrotation.shape[0] == 3
    return bestrotation

