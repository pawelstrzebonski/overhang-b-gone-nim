import stl
import triangle
import sphere_points
import algorithm
import arraymancer
import std/[strutils, parseopt]

proc runStlRotate*(infilename: string, angle: float32, num_rotations: uint) =
    ## Run STL rotation optimization on given STL file with specified angle threshold (for classifying as overhang)
    ## and number of candidate rotations
    doAssert(endsWith(infilename, ".stl"), "Filename must end in '.stl'")
    # Create output filename
    let outfilename = replace(infilename, ".stl", "_rotated.stl")
    # Parse input STL (assumes it's binary)
    #TODO: handle non-binary STL files
    var (tris, header) = parseStlBinary(infilename)
    # Generate list of potential rotation vectors
    let rotations = spherePoints(num_rotations)
    # Extract normals from triangles
    let normals = prepareNormalMatrix(tris)
    # Extract centers from triangles
    let centers = prepareCenterMatrix(tris)
    # Get surface areas of triangles
    let areas = prepareAreaMatrix(tris)
    # Run rotation optimization routine
    let bestrotation = naiveRotationOptimize(rotations, normals, areas, centers, angle)
    # Vector for original bottom surface normal
    let startVec = [0, 0, -1].toTensor().asType(float32)
    # Generate transformation matrix from normal space to optimized space
    let rotmat = getRotationMatrix(bestrotation, startVec)
    # Transform triangles in STL
    transformTriangles(rotmat, tris)
    # Re-export rotated mesh (copy/paste headers as from source file)
    writeStlBinary(outfilename, header, tris)

when isMainModule:
    # Default values
    var angle = 45f32
    var rotations = uint(100)
    var filename = ""
    # Parse command line arguments
    var p = initOptParser()
    while true:
        p.next()
        case p.kind
        of cmdEnd: break
        of cmdShortOption, cmdLongOption:
            if p.val == "":
                echo "Unknown option: ", p.key
            else:
                case p.key:
                    of "angle":
                        angle = parseFloat(p.val)
                    of "rotations":
                        rotations = parseUInt(p.val)
                    else:
                        echo "Unkown option/value: ", p.key, ", ", p.val
        of cmdArgument:
            filename = p.key
    runStlRotate(filename, angle, rotations)
