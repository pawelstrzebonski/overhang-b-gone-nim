import std/math
import arraymancer

proc spherePoints*(n: uint): Tensor[float32] =
    ## Generate a matrix representing a list of more-or-less evenly
    ## distributed points on a unit sphere's surface
    # https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere
    let indices = float32(0.5) +. arange[float32](float32(n))
    let phi = arccos(float32(1) -. ((float32(2)/float32(n)) * indices))
    let theta = float32(PI*float32(1.0+sqrt(5.0))) * indices
    let
        x = cos(theta) *. sin(phi)
        y = sin(theta) *. sin(phi)
        z = cos(phi)
    return stack(x, y, z, axis = 1)
