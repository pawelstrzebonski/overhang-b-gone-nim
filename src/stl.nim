import std/streams
import triangle


proc parseStlBinary*(filename: string): (seq[Triangle], array[80, uint8]) =
    ## Parse a binary STL file to a bunch of triangles
    var s = newFileStream(filename, fmRead)
    # First, get 80xuint8 header (not really used for anything)
    var header: array[80, uint8]
    for i in 0..<80:
        header[i] = s.readUint8()
    # Next, get uint32 count of triangles in file
    let numTriangles = s.readUint32()
    # Now everything afterwards should be triangles
    var tris = newSeq[Triangle](numTriangles)
    var i: uint32 = 0
    # While file is not finished and we have not read dictated
    # number of triangles...
    while not s.atEnd and i < numTriangles:
        # Each triangle will have normal vector and 3 vertices,
        # all of float32 type (12 float32's)
        # Read triangle data
        let n = [s.readFloat32(), s.readFloat32(), s.readFloat32()]
        let v1 = [s.readFloat32(), s.readFloat32(), s.readFloat32()]
        let v2 = [s.readFloat32(), s.readFloat32(), s.readFloat32()]
        let v3 = [s.readFloat32(), s.readFloat32(), s.readFloat32()]
        # Create our triangle type and save to array
        var tri: Triangle = Triangle(normal: n,
            vertex1: v1,
            vertex2: v2,
            vertex3: v3
        )
        tris[i] = tri
        # There is some uint16 attribute that we will ignore
        #TODO: save attribute so that it can be copy/pasted on export?
        discard s.readUint16()
        i = i+1
    # At the end of iteration, we expect to be both at end of file
    # and have correct number of triangles parsed
    doAssert(i == numTriangles and s.atEnd, "Bad STL file. Wrong # of triangles and/or bad file size.")
    # Close file
    s.close()
    # Return list of triangles and the header
    return (tris, header)

proc writeStlBinary*(filename: string, header: array[80, uint8],
        triangles: openArray[Triangle]) =
    ## Write list of triangles and header into a binary STL file
    # Open file for writing
    var s = newFileStream(filename, fmWrite)
    # Dump the header as-is
    for i in header:
        s.write(i)
    # Next goes the number of triangles as a uint32
    let numTriangles = uint32(len(triangles))
    s.write(numTriangles)
    # The rest of the file is all the triangles
    for triangle in triangles:
        # First the normal vector. followed by the three vertices
        for i in triangle.normal:
            s.write(i)
        for i in triangle.vertex1:
            s.write(i)
        for i in triangle.vertex2:
            s.write(i)
        for i in triangle.vertex3:
            s.write(i)
        # Afterwards is a uint16 attribute that is not used
        #TODO: copy/paste attribute from source file
        s.write(uint16(0))
    # Close file
    s.close()
