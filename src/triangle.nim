import std/math
import arraymancer

type
    Triangle* = object
        normal*: array[3, float32]
        vertex1*: array[3, float32]
        vertex2*: array[3, float32]
        vertex3*: array[3, float32]

func area*(t: Triangle): float32 =
    ## Calculate area of triangle
    #https://math.stackexchange.com/a/128999
    let
        x12 = t.vertex1[0]-t.vertex2[0]
        y12 = t.vertex1[1]-t.vertex2[1]
        z12 = t.vertex1[2]-t.vertex2[2]
        x13 = t.vertex1[0]-t.vertex3[0]
        y13 = t.vertex1[1]-t.vertex3[1]
        z13 = t.vertex1[2]-t.vertex3[2]
    return 0.5*sqrt(
        (y12*z13-z12*y13)^2 +
        (z12*x13-x12*z13)^2 +
        (x12*y13-y12*x13)^2
    )

func center*(t: Triangle): array[3, float32] =
    ## Calculate point within center of triangle (average of vertices really, centroid?)
    let
        x = (t.vertex1[0]+t.vertex2[0]+t.vertex3[0])/3
        y = (t.vertex1[1]+t.vertex2[1]+t.vertex3[1])/3
        z = (t.vertex1[2]+t.vertex2[2]+t.vertex3[2])/3
    return [x, y, z]

proc prepareNormalMatrix*(tris: openArray[Triangle]): Tensor[float32] =
    ## Create matrix of triangle normals
    var outMat = newTensor[float32](len(tris), 3)
    for i in tris.low..tris.high:
        for j in 0..2:
            outMat[i, j] = tris[i].normal[j]
    return outMat

proc prepareCenterMatrix*(tris: openArray[Triangle]): Tensor[float32] =
    ## Create matrix of triangle centers
    var outMat = newTensor[float32](len(tris), 3)
    for i in tris.low..tris.high:
        let c = center(tris[i])
        for j in 0..2:
            outMat[i, j] = c[j]
    return outMat

proc prepareAreaMatrix*(tris: openArray[Triangle]): Tensor[float32] =
    ## Create matrix/vector of triangle areas
    var outMat = newTensor[float32](len(tris))
    for i in tris.low..tris.high:
        outMat[i] = area(tris[i])
    return outMat

proc cross*(a, b: Tensor[float32]): Tensor[float32] =
    ## Vector cross product
    assert a.shape == b.shape
    let
        x = a[1]*b[2]-a[2]*b[1]
        y = a[2]*b[0]-a[0]*b[2]
        z = a[0]*b[1]-a[1]*b[0]
    var outVec = [x, y, z].toTensor
    return outVec

func norm2*(a: Tensor[float32]): float32 =
    ## Calculate vector norm-squared
    return dot(a, a)

proc getRotationMatrix*(a, b: Tensor[float32]): Tensor[float32] =
    ## Create a (non-unique) rotation matrix for transforming `a` into `b`
    # https://math.stackexchange.com/a/897677
    let v = cross(a, b)
    # Create constants for 1/0 of correct type
    let
        o = 1f32
        z = 0f32
    let ssc = [z, -v[2], v[1], v[2], z, -v[0], -v[1], v[0], z].toTensor(
        ).reshape(3, 3)
    let eye = [o, z, z, z, o, z, z, z, o].toTensor().reshape(3, 3)
    let R = eye +. ssc +. (ssc*ssc) * ((o-dot(a, b))/(norm2(v)))
    assert dot(R*a, b) > 0.95 # Sanity check that applying transform gets you close
    return R

proc copyToArray*(arr: var openArray[float32], vec: Tensor[float32]) =
    ## Copy values from Tensor to Array
    assert vec.shape[0] == len(arr)
    for i in arr.low..arr.high:
        arr[i] = vec[i]

proc transformTriangle*(rotation: Tensor[float32], triangle: var Triangle) =
    ## Apply rotation matrix to triangle
    assert rotation.shape[0] == 3 and rotation.shape[1] == 3
    copyToArray(triangle.normal, rotation*triangle.normal.toTensor())
    copyToArray(triangle.vertex1, rotation*triangle.vertex1.toTensor())
    copyToArray(triangle.vertex2, rotation*triangle.vertex2.toTensor())
    copyToArray(triangle.vertex3, rotation*triangle.vertex3.toTensor())

proc transformTriangles*(rotation: Tensor[float32], triangles: var openArray[Triangle]) =
    ## Apply rotation matrix to all triangles in list
    assert rotation.shape[0] == 3 and rotation.shape[1] == 3
    for i in triangles.low..triangles.high:
        transformTriangle(rotation, triangles[i])
