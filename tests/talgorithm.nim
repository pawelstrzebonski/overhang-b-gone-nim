import unittest
import ../src/algorithm
import ../src/triangle
import ../src/sphere_points
import arraymancer
include util

suite "verify algorithm":
    # Constants of right type
    let
        o = 1f32
        z = 0f32
        t = 2f32
    # Vectors we'll use a couple times
    let
        n = [z, z, o]
        v1 = [z, z, z]
        v2 = [o, z, z]
        v2t = [t, z, z]
        v3 = [z, o, z]
        v3t = [z, t, z]
    # Define a bunch of triangles
    let tri1 = triangle.Triangle(
        normal: n,
        vertex1: v1,
        vertex2: v2,
        vertex3: v3
    )
    let tri2 = triangle.Triangle(
        normal: n,
        vertex1: v1,
        vertex2: v2t,
        vertex3: v3
    )
    let tri3 = triangle.Triangle(
        normal: n,
        vertex1: v1,
        vertex2: v2,
        vertex3: v3t
    )
    let tri4 = triangle.Triangle(
        normal: n,
        vertex1: v1,
        vertex2: v2t,
        vertex3: v3t
    )
    let triarr = [tri1, tri2, tri3, tri4]
    # Create a bunch of points on sphere
    let rm = spherePoints(10)
    # Prepare inputs to algorithm
    let nm = triarr.prepareNormalMatrix()
    let am = triarr.prepareAreaMatrix()
    # Start running tests
    test "verify basic function":
        let bestrot = naiveRotationOptimize(rm, nm, am, 45f32)
        check(bestrot.shape[0] == 3)
        check(isapprox(dot(bestrot, bestrot), 1, 1e-4))
