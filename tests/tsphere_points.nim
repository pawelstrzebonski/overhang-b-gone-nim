import unittest
import ../src/sphere_points
import arraymancer
include util

suite "verify sphere sampling operations":
    let sp10 = spherePoints(10)
    let sp100 = spherePoints(100)
    # Start running tests
    test "verify point count":
        check(sp10.shape[0] == 10)
        check(sp10.shape[1] == 3)
        check(sp100.shape[0] == 100)
        check(sp100.shape[1] == 3)
    test "verify points on unit sphere":
        for i in 0..<sp10.shape[0]:
            check(isapprox(dot(sp10[i, _].squeeze(0), sp10[i, _].squeeze(0)), 1, 1e-4))
        for i in 0..<sp100.shape[0]:
            check(isapprox(dot(sp100[i, _].squeeze(0), sp100[i, _].squeeze(0)),
                    1, 1e-4))
