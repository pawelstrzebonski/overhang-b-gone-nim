import unittest
import ../src/triangle
include util
import arraymancer

suite "verify triangle/geometry operations":
    # Constants of right type
    let
        o = 1f32
        z = 0f32
        t = 2f32
    # Vectors we'll use a couple times
    let
        n = [z, z, o]
        v1 = [z, z, z]
        v2 = [o, z, z]
        v2t = [t, z, z]
        v3 = [z, o, z]
        v3t = [z, t, z]
    # Define a bunch of triangles
    let tri1 = triangle.Triangle(
        normal: n,
        vertex1: v1,
        vertex2: v2,
        vertex3: v3
    )
    let tri2 = triangle.Triangle(
        normal: n,
        vertex1: v1,
        vertex2: v2t,
        vertex3: v3
    )
    let tri3 = triangle.Triangle(
        normal: n,
        vertex1: v1,
        vertex2: v2,
        vertex3: v3t
    )
    let tri4 = triangle.Triangle(
        normal: n,
        vertex1: v1,
        vertex2: v2t,
        vertex3: v3t
    )
    let triarr = [tri1, tri2, tri3, tri4]
    # Start running tests
    test "verify center":
        let c = tri1.center()
        check(isapprox(c[0], 1/3, 1e-4))
        check(isapprox(c[1], 1/3, 1e-4))
        check(c[2] == 0)
    test "verify area":
        check(tri1.area() == 1/2)
        check(tri2.area() == 1)
        check(tri3.area() == 1)
        check(tri4.area() == 2)
    test "verify matrix shape":
        let nm = triarr.prepareNormalMatrix()
        check(nm.shape[0] == 4)
        check(nm.shape[1] == 3)
        let am = triarr.prepareAreaMatrix()
        check(am.shape[0] == 4)
    test "verify rotation matrix":
        let rm = getRotationMatrix(v2.toTensor, v3.toTensor)
        check(rm.shape[0] == 3)
        check(rm.shape[1] == 3)
        #TODO: this does not compile on Nix, libblas issue on test builds?
        #let v3rot=rm*v2.toTensor
        #check(isapprox(v3[0], v3rot[0], 1e-4))
        #check(isapprox(v3[1], v3rot[1], 1e-4))
        #check(isapprox(v3[2], v3rot[2], 1e-4))
    test "verify transforming triangles":
        let rm = getRotationMatrix(v2.toTensor, v3.toTensor)
        var t = tri1
        transformTriangle(rm, t)
        for i in 0..2:
            check(isapprox(t.vertex2[i], v3[i], 1e-4))
        var ts = triarr
        transformTriangles(rm, ts)
