## Helper functions for tests

func isapprox[T](a, b, l: T): bool =
    ## Approximate equality check (mostly for float comparisons)
    ## Are `a` and `b` within `l` of each-other?
    return abs(a-b) < l
